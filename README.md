# VLG_INVESTIMENTOS

A Comissão de Valores Mobiliários (CVM), é uma Autarquia vinculada ao Ministério da Economia, que tem o objetivo de fiscalizar, normatizar, disciplinar e desenvolver o mercado de valores. A CVM disponibiliza diariamente informes sobre Fundos de Investimentos no endereço http://dados.cvm.gov.br/dataset/fi-doc-inf_diario.

O seu desafio será criar um pipeline de dados, orquestrado pelo Airflow, que quando executado fará a captura de dados da CVM e carregará em um banco de dados PostgreSQL. Observando as diretivas:

    * Não duplicar dados ao salvar no banco (OK)
    * Validar a estrutura do dado recuperado e realizar seu tratamento antes da inserção (OK)
    * Agendar a execução diária do DAG para as 10:00 horas (OK)
    * Configurar o Airflow para realizar notificações por email em casos de falha na execução do DAG (OK)
    * (bônus) Código limpo e boa documentação (OK)
    * (bônus) Realizar commits pequenos e concisos (OK)
    * (bônus) Documentar as decisões e instruções de execução do projeto no arquivo README.md (OK)
    * (bônus) Executar o pipeline para uma carga inicial na base com os dados históricos da CVM (OK)


## RESUMO

Esse projeto foi desenvolvido em uma maquina virtual (Oracle - Linux -Ubuntu), nele foi instalado:

* Ambiente virtual
* pip
* PostgreSQL
* Apache airflow
* Bibliotecas:
    * Pandas
    * Beautifulsoup
    * Requests

## ESPECIFICACOES NECESSARIAS
* Python (3.6 >)
* Linux (Ubuntu)
* PostgreSQL

## PASSO A PASSO

### 1. INICIALIZACAO AMBIENTE VIRTUAL

**IMPORTANTE: O passo 1 deve ser repetido todas as vezes que o apache airflow quiser ser instanciado.**


Em um terminal va ate a pasta ambiente e ligue o ambiente, exporte o home e ligue o webserver.
```
cd airflow_vlg/
source bin/activate
export AIRFLOW_HOME=/path/vlg_investimentos/airflow_vlg
airflow db init 
airflow webserver -p 8080
```
Em outro terminal va ate a pasta ambiente e ligue o ambiente, exporte o home e ligue o scheduler.
```
cd airflow_vlg/
source bin/activate
export AIRFLOW_HOME=/path/vlg_investimentos/airflow_vlg
airflow scheduler
```
![imagem_inicializacao](inicializacao.png)


Feito isso aba o local host expedido pelo webserver.

### 2. POSTGRESQL

E Necessario que o postgres ja esteja instalado, apos instalado devera ser criado uma base de dados com o nome vlg e um usuario vlg admin com a senha airflow. Para isso execute em seu terminal:


```
sudo -i -u postgres
psql
```
Agora vc esta dentro do PostgreSQL.

```
create database vlg;
create user vlg_admin with encrypted password 'airflow';
grant all privileges on database vlg to vlg_admin;
```
(Esse codigo de criacao da tabela esta dentro do arquivo: POST_GRESQL_CONFIGURATIONS.txt)

O apache airflow ja foi configurado para colocar os dados nessa base de dados. Portanto deve funcionar, caso nao funcione, va ate connections (dentro do apache) e coloque a sua propria conexao com o seu banco de dados PostgreSQL.



### 3. APACHE AIRFLOW

Tudo foi devidamente configurado conforme solicitado, logo basta ligar a pipeline, e caso queira executar alguma instancia ou tarefa especifica em algum horario avulso, e necessario somente dar CLEAR e update. 

Mas algumas oberservacoes devem ser destacadas:

Ele esta rodando para apenas uma base(um mes), entre todos os meses la contidos (coloquei apenas um para demosntracao, mas caso queira-se rodar para todas basta seguir o passo a passo abaixo:

    * airflow_vlg/lib/python3.8/site-packages/airflow/example_dags/vlg_dag.py -abrir arquivos
        1. Dentro dele tera uma funcao chamada _data_mining
        2. La tera as explicacoes necessarias para rodar em todas as bases da cvm.)



![imagem_datamining](datamining.png)

Alem disso a deve-se tambem atualizar o email que o usuario quer receber o email.
(Deixarei o meu email como provedor somente para teste, depois retirarei por razoes de seguranca)
Para alterar o email de recebimento de erro basta seguir o passo a passo abaixo:

    * airflow_vlg/lib/python3.8/site-packages/airflow/example_dags/vlg_dag.py -abrir arquivos
        1. Dentro dele tera uma DAG com varias funcoes, mude o email em todas as secoes desejadas.
        2. La tera as explicacoes necessarias para rodar em todas as bases da cvm.
![imagem_inicializacao](email.png)

### 4. RODAR APACHE

A dag dentro do apache airflow ja esta configurada para rodar todos os dias as 10:00 AM horario de brasilia, porem caso queira-se rodar aleatoriamente basta:

>> CLICAR NA DAG >> GRAPH VIEW >> TASK INICIAL >> CLEAR

![imagem_airflow](apache_airflow_task.png)

### ARQUIVOS

* airflow_vlg: ambiente virtual
* POST_GRESQL_CONFIGURATIONS.txt: arquivo com codigo de criacao de tabela e usuario
* Imagens

## RESULTADOS

* Comprovacao da pipeline (DAG) via airflow foi criada - funionando o email em caso de falha.
![imagem_airflow](emails.png)

* Comprovacao Pipeline configurada e pronta para uso. (Codigos de extracao/criacao da tabela e insercao dos dados rodaram)
![imagem_airflow](1_run.png)
![imagem_airflow](2_run.png)
![imagem_airflow](3_run.png)
![imagem_airflow](4_run.png)
![imagem_airflow](5_run.png)

* Comprovacao criacao de Bando de dados PostgreSQL
![imagem_airflow](comprovacao.png)


* Comprovacao Insercao dos Dados no Banco de Dados.
![imagem_airflow](registros.png)



## Direitos e Licencas e Referencias

Apache License - Version 2.0, January 2004 - http://www.apache.org/licenses/
Python 3.8.5 - https://docs.python.org/release/3.8.5/whatsnew/changelog.html#changelog
CVM dataset - http://dados.cvm.gov.br/dataset/fi-doc-inf_diario / http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/DADOS/



